//
//  UIBezierPath+RoundedCornerPath.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <UIKit/UIKit.h>

//################################################################################
@interface UIBezierPath (RoundedCornerPath)

//################################################################################
+ (UIBezierPath *)createRoundedCornerPath:(CGRect)rect radiuses:(CGRect)radiuses;

//################################################################################
@end
//################################################################################
