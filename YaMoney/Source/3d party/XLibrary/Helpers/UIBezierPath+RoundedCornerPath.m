//
//  UIBezierPath+RoundedCornerPath.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "UIBezierPath+RoundedCornerPath.h"

//################################################################################
@implementation UIBezierPath (RoundedCornerPath)

//################################################################################
+ (UIBezierPath *)createRoundedCornerPath:(CGRect)rect radiuses:(CGRect)radiuses
{
    UIBezierPath * path = [UIBezierPath bezierPath];
    
    // get the 4 corners of the rect
    CGPoint topLeft = CGPointMake(rect.origin.x, rect.origin.y);
    CGPoint topRight = CGPointMake(rect.origin.x + rect.size.width, rect.origin.y);
    CGPoint bottomRight = CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height);
    CGPoint bottomLeft = CGPointMake(rect.origin.x, rect.origin.y + rect.size.height);
    
    CGPoint p1 = CGPointZero;
    CGPoint p2 = CGPointZero;
    CGFloat radiusTopLeft = radiuses.origin.x;
    CGFloat radiusTopRight = radiuses.origin.y;
    CGFloat radiusBottomRight = radiuses.size.width;
    CGFloat radiusBottomLeft = radiuses.size.height;
    
    // move to top left
    p1 = CGPointMake(topLeft.x + radiusTopLeft, topLeft.y);
    [path moveToPoint:p1];
    
    // add top line
    p1 = CGPointMake(topRight.x - radiusTopRight, topRight.y);
    [path addLineToPoint:p1];
    
    // add top right curve
    p1 = CGPointMake(topRight.x, topRight.y + radiusTopRight);
    p2 = CGPointMake(topRight.x, topRight.y);
    [path addQuadCurveToPoint:p1 controlPoint:p2];
    
    // add right line
    p1 = CGPointMake(bottomRight.x, bottomRight.y - radiusBottomRight);
    [path addLineToPoint:p1];
    
    // add bottom right curve
    p1 = CGPointMake(bottomRight.x - radiusBottomRight, bottomRight.y);
    p2 = CGPointMake(bottomRight.x, bottomRight.y);
    [path addQuadCurveToPoint:p1 controlPoint:p2];
    
    // add bottom line
    p1 = CGPointMake(bottomLeft.x + radiusBottomLeft, bottomLeft.y);
    [path addLineToPoint:p1];
    
    // add bottom left curve
    p1 = CGPointMake(bottomLeft.x, bottomLeft.y - radiusBottomLeft);
    p2 = CGPointMake(bottomLeft.x, bottomLeft.y);
    [path addQuadCurveToPoint:p1 controlPoint:p2];
    
    // add left line
    p1 = CGPointMake(topLeft.x, topLeft.y + radiusTopLeft);
    [path addLineToPoint:p1];
    
    // add top left curve
    p1 = CGPointMake(topLeft.x + radiusTopLeft, topLeft.y);
    p2 = CGPointMake(topLeft.x, topLeft.y);
    [path addQuadCurveToPoint:p1 controlPoint:p2];
    
    [path closePath];
    return path;
}

//################################################################################
@end
//################################################################################
