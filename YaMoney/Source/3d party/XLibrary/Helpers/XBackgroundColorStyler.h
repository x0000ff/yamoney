//
//  XBackgroundColorStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXBackgroundColor_Key =     @"kXBackgroundColor_Key";

//################################################################################
@interface XBackgroundColorStyler : XStylerHelper

//################################################################################
@end
//################################################################################
