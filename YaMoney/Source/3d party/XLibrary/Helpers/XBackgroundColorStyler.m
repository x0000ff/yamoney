//
//  XBackgroundColorStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XBackgroundColorStyler.h"

//################################################################################
@implementation XBackgroundColorStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyKey = kXBackgroundColor_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setBackgroundColor:) forProperty:propertyKey]) {
        
        UIColor * bkgColor = styleDictionary[propertyKey];
        if (bkgColor) {
            [(id)objectToStyle setBackgroundColor:bkgColor];
        }
    }
}

//################################################################################
@end
//################################################################################
