//
//  XBorderStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXBorderWidth_Key =         @"kXBorderWidth_Key";
static NSString * const kXBorderColor_Key =         @"kXBorderColor_Key";

//################################################################################
@interface XBorderStyler : XStylerHelper

//################################################################################
@end
//################################################################################
