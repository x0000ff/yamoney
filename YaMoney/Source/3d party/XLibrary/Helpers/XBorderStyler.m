//
//  XBorderStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XBorderStyler.h"
#import "XView.h"

//################################################################################
@implementation XBorderStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyKey = kXBorderWidth_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setBorderWidth:) forProperty:propertyKey]) {
        
        NSNumber * borderWidth = styleDictionary[propertyKey];
        if (borderWidth) {
            [(id)objectToStyle setBorderWidth:borderWidth.floatValue];
        }
    }
    
    propertyKey = kXBorderColor_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setBorderColor:) forProperty:propertyKey]) {
        
        UIColor * borderColor = styleDictionary[propertyKey];
        if (borderColor) {
            [(XView *)objectToStyle setBorderColor:borderColor];
        }
    }
}

//################################################################################
@end
//################################################################################
