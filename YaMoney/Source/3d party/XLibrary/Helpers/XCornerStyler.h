//
//  XCornerStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXCornerRoundValue_Key =    @"kXCornerRoundValue_Key";

static NSString * const kXTopLeftCornerRadius =     @"kXTopLeftCornerRadius";
static NSString * const kXTopRightCornerRadius =    @"kXTopRightCornerRadius";
static NSString * const kXBottomRightCornerRadius = @"kXBottomRightCornerRadius";
static NSString * const kXBottomLeftCornerRadius =  @"kXBottomLeftCornerRadius";

//################################################################################
@interface XCornerStyler : XStylerHelper

//################################################################################
@end
//################################################################################
