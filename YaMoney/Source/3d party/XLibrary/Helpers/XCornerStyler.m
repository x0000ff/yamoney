//
//  XCornerStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XCornerStyler.h"
#import "XView.h"

//################################################################################
@implementation XCornerStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyKey = kXCornerRoundValue_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setCornerRadius:) forProperty:propertyKey]) {
        
        NSNumber * value = styleDictionary[propertyKey];
        if (value) {
            [(CALayer *)objectToStyle setCornerRadius:value.floatValue];
        }
    }

    // Top Left
    propertyKey = kXTopLeftCornerRadius;
    if ([self isStylable:objectToStyle bySelector:@selector(setTopLeftCornerRadius:) forProperty:propertyKey]) {
        
        NSNumber * value = styleDictionary[propertyKey];
        if (value) {
            [(id)objectToStyle setTopLeftCornerRadius:value.floatValue];
        }
    }

    // Top Right
    propertyKey = kXTopRightCornerRadius;
    if ([self isStylable:objectToStyle bySelector:@selector(setTopRightCornerRadius:) forProperty:propertyKey]) {
        
        NSNumber * value = styleDictionary[propertyKey];
        if (value) {
            [(id)objectToStyle setTopRightCornerRadius:value.floatValue];
        }
    }
    
    // Bottom Left
    propertyKey = kXBottomLeftCornerRadius;
    if ([self isStylable:objectToStyle bySelector:@selector(setBottomLeftCornerRadius:) forProperty:propertyKey]) {
        
        NSNumber * value = styleDictionary[propertyKey];
        if (value) {
            [(id)objectToStyle setBottomLeftCornerRadius:value.floatValue];
        }
    }
    
    // Bottom Right
    propertyKey = kXBottomRightCornerRadius;
    if ([self isStylable:objectToStyle bySelector:@selector(setBottomRightCornerRadius:) forProperty:propertyKey]) {
        
        NSNumber * value = styleDictionary[propertyKey];
        if (value) {
            [(id)objectToStyle setBottomRightCornerRadius:value.floatValue];
        }
    }
}

//################################################################################
@end
//################################################################################
