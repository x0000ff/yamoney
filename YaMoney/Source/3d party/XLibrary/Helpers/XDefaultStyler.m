//
//  XDefaultStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XDefaultStyler.h"
#import "XStyles.h"

//################################################################################
@implementation XDefaultStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyKey = kXOpacity_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setAlpha:) forProperty:propertyKey]) {
        
        NSNumber * value = styleDictionary[propertyKey];
        if (value) {
            [(id)objectToStyle setAlpha:value.floatValue];
        }
    }
}

//################################################################################
@end
//################################################################################
