//
//  XFontStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXFontName_Key =            @"kXFontName_Key";
static NSString * const kXFontSize_Key =            @"kXFontSize_Key";

static NSString * const kXTextColor_Key =           @"kXTextColor_Key";

//################################################################################
@interface XFontStyler : XStylerHelper

//################################################################################
@end
//################################################################################
