//
//  XFontStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XFontStyler.h"

//################################################################################
@implementation XFontStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyFontNameKey = kXFontName_Key;
    NSString * propertyFontSizeKey = kXFontSize_Key;

    if ([self isStylable:objectToStyle bySelector:@selector(setFont:) forProperties:@[propertyFontNameKey, propertyFontSizeKey]]) {
        
        NSString * fontName = styleDictionary[propertyFontNameKey];
        fontName = fontName ? fontName : [(id)[(id)objectToStyle font] familyName];
        
        NSNumber * fontSize = styleDictionary[propertyFontSizeKey];
        fontSize = fontSize ? fontSize : @([(id)[(id)objectToStyle font] pointSize]);
        
        if (fontSize && fontName) {
            
            UIFont * font = [UIFont fontWithName:fontName size:fontSize.floatValue];
            if (font.pointSize > 0) {
                [(id)objectToStyle setFont:font];
            }
        }
    }
    
    // Text Color
    NSString * propertyKey = kXTextColor_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setTextColor:) forProperty:propertyKey]) {
        
        UIColor * value = styleDictionary[propertyKey];
        if (value) {
            [(id)objectToStyle setTextColor:value];
        }
    }
}

//################################################################################
@end
//################################################################################
