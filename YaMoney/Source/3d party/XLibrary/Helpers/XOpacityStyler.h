//
//  XOpacityStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXOpacity_Key =           @"kXOpacity_Key";

//################################################################################
@interface XOpacityStyler : XStylerHelper

//################################################################################
@end
//################################################################################
