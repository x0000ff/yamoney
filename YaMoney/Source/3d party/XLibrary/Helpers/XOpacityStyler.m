//
//  XOpacityStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XOpacityStyler.h"

//################################################################################
@implementation XOpacityStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyKey = kXOpacity_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setAlpha:) forProperty:propertyKey]) {
        
        NSNumber * opacity = styleDictionary[propertyKey];
        if (opacity) {
            [(id)objectToStyle setAlpha:opacity.floatValue];
        }
    }
}

//################################################################################
@end
//################################################################################
