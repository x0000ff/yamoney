//
//  XStatableBackgroundColorStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXBackgroundColorNormal_Key =       @"kXBackgroundColorNormal_Key";
static NSString * const kXBackgroundColorHighlighted_Key =  @"kXBackgroundColorHighlighted_Key";
static NSString * const kXBackgroundColorDisabled_Key =     @"kXBackgroundColorDisabled_Key";
static NSString * const kXBackgroundColorSelected_Key =     @"kXBackgroundColorSelected_Key";

//################################################################################
@interface XStatableBackgroundColorStyler : XStylerHelper

//################################################################################
@end
//################################################################################
