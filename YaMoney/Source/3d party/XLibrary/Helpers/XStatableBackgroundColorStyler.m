//
//  XStatableBackgroundColorStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XStatableBackgroundColorStyler.h"

//################################################################################
@implementation XStatableBackgroundColorStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * colorNormalKey = kXBackgroundColorNormal_Key;
    NSString * colorSelectedKey = kXBackgroundColorSelected_Key;
    NSString * colorDisabledKey = kXBackgroundColorDisabled_Key;
    NSString * colorHighlightedKey = kXBackgroundColorHighlighted_Key;

    if ([self isStylable:objectToStyle bySelector:@selector(setBackgroundImage:forState:) forProperties:@[colorNormalKey, colorSelectedKey, colorDisabledKey, colorHighlightedKey]]) {
        
        if ([objectToStyle respondsToSelector:@selector(setBackgroundColor:)]) {
            [(id)objectToStyle setBackgroundColor:nil];
        }

        UIColor * colorNormal = styleDictionary[colorNormalKey];
        UIColor * colorSelected = styleDictionary[colorSelectedKey];
        UIColor * colorDisabled = styleDictionary[colorDisabledKey];
        UIColor * colorHighlighted = styleDictionary[colorHighlightedKey];
        
        if (colorNormal) {
            [(id)objectToStyle setBackgroundImage:[UIImage imageWithColor:colorNormal] forState:UIControlStateNormal];
        }
        if (colorSelected) {
            [(id)objectToStyle setBackgroundImage:[UIImage imageWithColor:colorSelected] forState:UIControlStateSelected];
        }
        if (colorDisabled) {
            [(id)objectToStyle setBackgroundImage:[UIImage imageWithColor:colorDisabled] forState:UIControlStateDisabled];
        }
        if (colorHighlighted) {
            [(id)objectToStyle setBackgroundImage:[UIImage imageWithColor:colorHighlighted] forState:UIControlStateHighlighted];
        }
    }

}

//################################################################################
@end
//################################################################################
