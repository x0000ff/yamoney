//
//  XStylerHelper.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylable.h"

//################################################################################
@interface XStylerHelper : NSObject

//################################################################################
+ (void) style:(NSObject<XStylable> *)objectToStyle;
+ (void) style:(NSObject *)objectToStyle withStyleFrom:(NSObject<XStylable> *)objectWithStyle;

+ (NSDictionary *) flattenStyleDictionaryForObject:(NSObject<XStylable> *)objectToStyle;
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary;

+ (BOOL) isStylable:(NSObject *)objectToStyle bySelector:(SEL)stylingSelector forProperties:(NSArray *)styleProperties;
+ (BOOL) isStylable:(NSObject *)objectToStyle bySelector:(SEL)stylingSelector forProperty:(NSString *)styleProperty;

+ (void) logUnstylable:(NSObject *)objectToStyle;
+ (void) logUnstylable:(NSObject *)objectToStyle forProperty:(NSString *)styleProperty;

//################################################################################
@end
//################################################################################
