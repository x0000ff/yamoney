//
//  XStylerHelper.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XStylerHelper.h"
#import "NSString+XUtilities.h"
#import "XStyles.h"

//#define STYLER_DEBUG

//################################################################################
@implementation XStylerHelper

//################################################################################
+ (void) style:(NSObject<XStylable> *)objectToStyle
{
    [self style:objectToStyle withStyleFrom:objectToStyle];
}

//################################################################################
+ (void) style:(NSObject *)objectToStyle withStyleFrom:(NSObject<XStylable> *)objectWithStyle
{
    NSDictionary * style = [self.class flattenStyleDictionaryForObject:objectWithStyle];
    
    if (style && (style.count > 0)) {
        [self style:objectToStyle withFlattenStyleDictionary:style];
    }
}

//################################################################################
+ (NSDictionary *) flattenStyleDictionaryForObject:(NSObject<XStylable> *)objectToStyle
{
    NSMutableDictionary * flattenStyleDictionary = [@{} mutableCopy];
    NSString * stylesString = [objectToStyle styleClass];
    
    if (!stylesString) return flattenStyleDictionary;
    NSArray * styles = [stylesString componentsSeparatedByString:@" "];
    
    for (NSString * styleClass in styles) {
        if (![styleClass nonEmpty]) continue;
        
        NSDictionary * styleDictionary = [XStyles styleWithName:styleClass];
        if (!styleDictionary)
        {
            [self logStyleClassNotFound:objectToStyle styleClass:styleClass];
            continue;
        }
        
        [flattenStyleDictionary addEntriesFromDictionary:styleDictionary];
    }

    return flattenStyleDictionary;
}

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    // Should be overwritten in children
}

//################################################################################
+ (BOOL) isStylable:(NSObject *)objectToStyle bySelector:(SEL)stylingSelector forProperty:(NSString *)styleProperty
{
    return [self isStylable:objectToStyle bySelector:stylingSelector forProperties:(styleProperty ? @[styleProperty] : @[])];
}

//################################################################################
+ (BOOL) isStylable:(NSObject *)objectToStyle bySelector:(SEL)stylingSelector forProperties:(NSArray *)styleProperties
{
    if (!stylingSelector) return NO;
    if (styleProperties.count == 0) return NO;
    
    if (![objectToStyle respondsToSelector:stylingSelector]) {
        for (NSString * styleProperty in styleProperties) {
            [self logUnstylable:objectToStyle forProperty:styleProperty];
        }
        return NO;
    }
    
    return YES;
}

//################################################################################
+ (void) logUnstylable:(NSObject *)objectToStyle forProperty:(NSString *)styleProperty
{
#ifdef STYLER_DEBUG
    NSString * byPropertyPart = styleProperty ? [NSString stringWithFormat:@" :: byProperty \"%@\"", styleProperty] : @"";
    NSLog(@" [ %@ ] couldn't be styled by < %@ > %@", NSStringFromClass(objectToStyle.class), NSStringFromClass(self), byPropertyPart);
#endif
}

//################################################################################
+ (void) logUnstylable:(NSObject *)objectToStyle
{
#ifdef STYLER_DEBUG
    NSLog(@" [ %@ ] couldn't be styled by < %@ >", NSStringFromClass(objectToStyle.class), NSStringFromClass(self));
#endif
}

//################################################################################
+ (void) logStyleClassNotFound:(NSObject<XStylable> *)objectToStyle styleClass:(NSString *)styleClass
{
#ifdef STYLER_DEBUG
    NSLog(@"WARNING: Style \"%@\" not found for object [ %@ ]", styleClass, NSStringFromClass(objectToStyle.class));
#endif
}

//################################################################################
@end
//################################################################################
