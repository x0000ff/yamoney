//
//  XTextAlignmentStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXTextAlignment_Key = @"kXTextAlignment_Key"; // left | center | right | justified | natural

static NSString * const kXTextAlignmentLeft_ValueKey = @"left";
static NSString * const kXTextAlignmentRight_ValueKey = @"center";
static NSString * const kXTextAlignmentCenter_ValueKey = @"right";
static NSString * const kXTextAlignmentJustified_ValueKey = @"justified";
static NSString * const kXTextAlignmentNatural_ValueKey = @"natural";

//################################################################################
@interface XTextAlignmentStyler : XStylerHelper

//################################################################################
@end
//################################################################################
