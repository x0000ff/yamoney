//
//  XTextAlignmentStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XTextAlignmentStyler.h"

//################################################################################
@implementation XTextAlignmentStyler

//################################################################################
+ (NSTextAlignment) textAlignmentValueFrom:(NSString *)textAlignmentValueString
{
    NSTextAlignment defaultValue = NSTextAlignmentCenter;
    if (![textAlignmentValueString nonEmpty]) return defaultValue;
    
    if ([textAlignmentValueString isEqualToString:kXTextAlignmentCenter_ValueKey]) return NSTextAlignmentCenter;
    if ([textAlignmentValueString isEqualToString:kXTextAlignmentLeft_ValueKey]) return NSTextAlignmentLeft;
    if ([textAlignmentValueString isEqualToString:kXTextAlignmentRight_ValueKey]) return NSTextAlignmentRight;
    if ([textAlignmentValueString isEqualToString:kXTextAlignmentJustified_ValueKey]) return NSTextAlignmentJustified;
    if ([textAlignmentValueString isEqualToString:kXTextAlignmentNatural_ValueKey]) return NSTextAlignmentNatural;
    
    return defaultValue;
}
//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyKey = kXTextAlignment_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setTextAlignment:) forProperty:propertyKey]) {
        
        NSString * textAlignmentStringValue = styleDictionary[propertyKey];
        if (textAlignmentStringValue) {
            NSTextAlignment textAlignmentValue = [self textAlignmentValueFrom:textAlignmentStringValue];
            [(id)objectToStyle setTextAlignment:textAlignmentValue];
        }
    }
}

//################################################################################
@end
//################################################################################
