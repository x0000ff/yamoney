//
//  XTintStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XTintStyler.h"
#import "XStyles.h"

//################################################################################
@implementation XTintStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * propertyKey = kXTintColor_Key;
    if ([self isStylable:objectToStyle bySelector:@selector(setTintColor:) forProperty:propertyKey]) {
      
        UIColor * tintColor = styleDictionary[propertyKey];
        if (tintColor) {
            [(id)objectToStyle setTintColor:tintColor];
        }
    }
}

//################################################################################
@end
//################################################################################
