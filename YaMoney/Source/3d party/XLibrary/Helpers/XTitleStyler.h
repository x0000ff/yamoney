//
//  XTitleStyler.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>
#import "XStylerHelper.h"

//################################################################################
static NSString * const kXTextColorNormal_Key =         @"kXTextColorNormal_Key";
static NSString * const kXTextColorHighlighted_Key =    @"kXTextColorHighlighted_Key";
static NSString * const kXTextColorDisabled_Key =       @"kXTextColorDisabled_Key";
static NSString * const kXTextColorSelected_Key =       @"kXTextColorSelected_Key";

//################################################################################
@interface XTitleStyler : XStylerHelper

//################################################################################
@end
//################################################################################
