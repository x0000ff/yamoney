//
//  XTitleStyler.m
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import "XTitleStyler.h"
#import "XStyles.h"

//################################################################################
@implementation XTitleStyler

//################################################################################
+ (void) style:(NSObject *)objectToStyle withFlattenStyleDictionary:(NSDictionary *)styleDictionary
{
    NSString * colorNormalKey = kXTextColorNormal_Key;
    NSString * colorSelectedKey = kXTextColorSelected_Key;
    NSString * colorDisabledKey = kXTextColorDisabled_Key;
    NSString * colorHighlightedKey = kXTextColorHighlighted_Key;

    if ([self isStylable:objectToStyle bySelector:@selector(setTitleColor:forState:) forProperties:@[colorNormalKey, colorSelectedKey, colorDisabledKey, colorHighlightedKey]]) {
        
        UIColor * colorNormal = styleDictionary[colorNormalKey];
        UIColor * colorSelected = styleDictionary[colorSelectedKey];
        UIColor * colorDisabled = styleDictionary[colorDisabledKey];
        UIColor * colorHighlighted = styleDictionary[colorHighlightedKey];

        if (colorNormal) {
            [(id)objectToStyle setTitleColor:colorNormal forState:UIControlStateNormal];
        }
        if (colorSelected) {
            [(id)objectToStyle setTitleColor:colorSelected forState:UIControlStateSelected];
        }
        if (colorDisabled) {
            [(id)objectToStyle setTitleColor:colorDisabled forState:UIControlStateDisabled];
        }
        if (colorHighlighted) {
            [(id)objectToStyle setTitleColor:colorHighlighted forState:UIControlStateHighlighted];
        }
    }
}

//################################################################################
@end
//################################################################################
