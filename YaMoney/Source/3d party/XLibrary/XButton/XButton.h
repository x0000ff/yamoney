//
//  XButton.h
//
//  Created by Konstantin Portnov on 11/12/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import <UIKit/UIKit.h>
#import "XStylable.h"

//################################################################################
@interface XButton : UIButton <XStylable>

//################################################################################
@property (strong, nonatomic) NSString * styleClass;

@property (assign, nonatomic) CGFloat borderWidth;
@property (strong, nonatomic) UIColor * borderColor;

//################################################################################
- (void) setCornerRadius:(CGFloat)cornerRadius;

//################################################################################
@end
//################################################################################
