//
//  XButton.m
//
//  Created by Konstantin Portnov on 11/12/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import "XButton.h"
#import "XStyles.h"
#import "UIBezierPath+RoundedCornerPath.h"

//################################################################################
@implementation XButton

//################################################################################
- (void) setStyleClass:(NSString *)styleClass
{
    if ([_styleClass isEqualToString:styleClass]) return;
    
    _styleClass = styleClass;
    [self stylize];
}

//################################################################################
- (id) initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        [self initialCustomization];
    }
    
    return self;
}

//################################################################################
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialCustomization];
    }
    
    return self;
}

//################################################################################
- (void) initialCustomization {
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    _borderWidth = 0;
    _borderColor = nil;
}

//################################################################################
#pragma mark - Setters / Getters

//################################################################################
- (void) setCornerRadius:(CGFloat)cornerRadius {

    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = cornerRadius > 0;
}

//################################################################################
- (void) setBorderColor:(UIColor *)borderColor {

    _borderColor = borderColor;
    self.layer.borderColor = borderColor ? borderColor.CGColor : nil;
}

//################################################################################
- (void) setBorderWidth:(CGFloat)borderWidth {
    
    _borderWidth = borderWidth;
    self.layer.borderWidth = borderWidth;
}

//################################################################################
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGRect rect = contentRect;

    rect = CGRectInset(rect, self.borderWidth, self.borderWidth);
    rect = [super titleRectForContentRect:rect];

    return rect;
}

//################################################################################
#pragma mark - Stylizable

//################################################################################
+ (NSArray *) stylers
{
    return @[
             XBackgroundColorStyler.class,
             XStatableBackgroundColorStyler.class,
             XOpacityStyler.class,
             XTintStyler.class,
             XBorderStyler.class,
             XCornerStyler.class,
             XFontStyler.class,
             XTitleStyler.class,
             ];
}

//################################################################################
- (void) stylize {
    
    [XFontStyler style:self.titleLabel withStyleFrom:self];
    [XTextAlignmentStyler style:self.titleLabel withStyleFrom:self];

    for (Class stylerClass in [self.class stylers]) {
        [stylerClass style:self];
    }
}

//################################################################################
@end
//################################################################################