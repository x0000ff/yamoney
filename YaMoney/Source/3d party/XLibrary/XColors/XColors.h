//
//  XColors.h
//
//  Created by Konstantin Portnov on 11/15/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>

//################################################################################
@interface XColors : NSObject

//################################################################################
+ (UIColor *) sunglow;
+ (UIColor *) lilyWhite;
+ (UIColor *) wildSand;
+ (UIColor *) gamboge;
+ (UIColor *) kournikova;
+ (UIColor *) schoolbusYellow;
+ (UIColor *) bunker;
+ (UIColor *) midGray;

//################################################################################
@end
//################################################################################
