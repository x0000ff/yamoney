//
//  XColors.m
//
//  Created by Konstantin Portnov on 11/15/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import "XColors.h"

//################################################################################
@implementation XColors

//################################################################################
#pragma mark - App Colors

//################################################################################
+ (UIColor *) sunglow         { return UIColorFromHexValue(0xFED433); }
+ (UIColor *) lilyWhite       { return UIColorFromHexValue(0xECEBEA); }
+ (UIColor *) wildSand        { return UIColorFromHexValue(0xF6F5F3); }
+ (UIColor *) gamboge         { return UIColorFromHexValue(0xE79E00); }
+ (UIColor *) kournikova      { return UIColorFromHexValue(0xFFE87B); }
+ (UIColor *) schoolbusYellow { return UIColorFromHexValue(0xFFD800); }
+ (UIColor *) bunker          { return UIColorFromHexValue(0x2D2D2D); }
+ (UIColor *) midGray         { return UIColorFromHexValue(0x575F6B); }

//################################################################################
@end
//################################################################################
