//
//  XFlatColors.h
//
//  Created by Konstantin Portnov on 11/15/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>

//################################################################################
@interface XFlatColors : NSObject

//################################################################################
#pragma mark - Flat Colors

//################################################################################
+ (UIColor *) turquoiseColor;
+ (UIColor *) emeraldColor;
+ (UIColor *) peterRiverColor;
+ (UIColor *) amethystColor;
+ (UIColor *) wetAsphaltColor;
+ (UIColor *) greenSeaColor;
+ (UIColor *) nephritisColor;
+ (UIColor *) belizeHoleColor;
+ (UIColor *) wisteriaColor;
+ (UIColor *) midnightBlueColor;
+ (UIColor *) sunFlowerColor;
+ (UIColor *) carrotColor;
+ (UIColor *) alizarinColor;
+ (UIColor *) cloudsColor;
+ (UIColor *) concreteColor;
+ (UIColor *) orangeColor;
+ (UIColor *) pumpkinColor;
+ (UIColor *) pomegranateColor;
+ (UIColor *) silverColor;
+ (UIColor *) asbestosColor;

//################################################################################
@end
//################################################################################
