//
//  XFlatColors.m
//
//  Created by Konstantin Portnov on 11/15/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import "XFlatColors.h"

//################################################################################
@implementation XFlatColors

//################################################################################
#pragma mark - Flat Colors

//################################################################################
+ (UIColor *) turquoiseColor { return UIColorFromHexValue(0x1abc9c); };
+ (UIColor *) emeraldColor { return UIColorFromHexValue(0x2ecc71); };
+ (UIColor *) peterRiverColor { return UIColorFromHexValue(0x3498db);};
+ (UIColor *) amethystColor { return UIColorFromHexValue(0x9b59b6); };
+ (UIColor *) wetAsphaltColor { return UIColorFromHexValue(0x34495e); };
+ (UIColor *) greenSeaColor { return UIColorFromHexValue(0x2ea083); };
+ (UIColor *) nephritisColor { return UIColorFromHexValue(0x3cae55); };
+ (UIColor *) belizeHoleColor { return UIColorFromHexValue(0x2f7fbe); };
+ (UIColor *) wisteriaColor { return UIColorFromHexValue(0x8a42b3); };
+ (UIColor *) midnightBlueColor { return UIColorFromHexValue(0x2d3e51); };
+ (UIColor *) sunFlowerColor { return UIColorFromHexValue(0xf0c500); };
+ (UIColor *) carrotColor { return UIColorFromHexValue(0xe27f00); };
+ (UIColor *) alizarinColor { return UIColorFromHexValue(0xe24e35); };
+ (UIColor *) cloudsColor { return UIColorFromHexValue(0xecf0f1); };
+ (UIColor *) concreteColor { return UIColorFromHexValue(0x96a5a6); };
+ (UIColor *) orangeColor { return UIColorFromHexValue(0xf09d00); };
+ (UIColor *) pumpkinColor { return UIColorFromHexValue(0xcf5600); };
+ (UIColor *) pomegranateColor { return UIColorFromHexValue(0xbc3b24); };
+ (UIColor *) silverColor { return UIColorFromHexValue(0xbdc3c7); };
+ (UIColor *) asbestosColor { return UIColorFromHexValue(0x808c8d); };

//################################################################################
@end
//################################################################################
