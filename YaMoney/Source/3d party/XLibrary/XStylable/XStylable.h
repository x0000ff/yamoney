//
//  NSObject+XStylable.h
//
//  Created by Konstantin Portnov on 4/26/14.
//  Copyright (c) 2014 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>

//################################################################################
@protocol XStylable <NSObject>

- (void) stylize;
- (NSString *) styleClass;
+ (NSArray *) stylers;

//################################################################################
@end
//################################################################################
