//
//  XStyles.h
//
//  Created by Konstantin Portnov on 11/15/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>

#import "XStatableBackgroundColorStyler.h"
#import "XBackgroundColorStyler.h"
#import "XOpacityStyler.h"
#import "XTintStyler.h"
#import "XBorderStyler.h"
#import "XCornerStyler.h"
#import "XFontStyler.h"
#import "XTitleStyler.h"
#import "XTextAlignmentStyler.h"

//################################################################################
@interface XStyles : NSObject

//################################################################################
@property (strong, nonatomic) NSDictionary * styles;

//################################################################################
+ (NSDictionary *)styleWithName:(NSString *)styleName;
+ (UIFont *)fontForStyle:(NSString *)styleName;

//################################################################################
+ (XStyles *) shared;

//################################################################################
@end
//################################################################################
