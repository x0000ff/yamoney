//
//  XStyles.m
//
//  Created by Konstantin Portnov on 11/15/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import "XStyles.h"
#import "XColors.h"

//################################################################################
@implementation XStyles

//################################################################################
+ (XStyles *) shared
{
    static id _shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[self alloc] init];
        [_shared loadStyles];
        
    });
    
    return _shared;
}

//################################################################################
- (void) loadStyles
{
    self.styles = [self.class allStyles];
}

//################################################################################
+ (NSDictionary *)styleWithName:(NSString *)styleName
{
    NSDictionary * allStyle = [self shared].styles;
    NSDictionary * styleD = [allStyle objectForKey:styleName];

  if (!styleD) {
    
    NSString * noStyleErrorMessage = [NSString stringWithFormat:@"Style '%@' hasn't been found", styleName];
    NSLog(noStyleErrorMessage);
    
  }
    return styleD;
}

//################################################################################
+ (UIFont *)fontForStyle:(NSString *)styleName
{
    NSDictionary * allStyle = [self shared].styles;
    NSDictionary * styleD = [allStyle objectForKey:styleName];

    UIFont * font = nil;
    NSString * fontName = styleD[kXFontName_Key];
    NSNumber * fontSize = styleD[kXFontSize_Key];
    if (fontSize && fontName) font = [UIFont fontWithName:fontName size:fontSize.floatValue];

    return font;
}

//################################################################################
#pragma mark - Font Names

//################################################################################
+ (NSDictionary *) allStyles {
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"

    // Font
    NSString * kFontNormal = @"HelveticaNeue";
    NSString * kFontLight = @"HelveticaNeue-Light";
    NSString * kFontMedium = @"HelveticaNeue-Medium";
    NSString * kFontBold = @"HelveticaNeue-Bold";

    // Corner
    NSNumber * kCornerNone = @(0);
  
    // Border
    NSNumber * kBorderNone = @(0);
  
    UIColor * kClearColor = [UIColor clearColor];
    
    return @{
             
             @"round-20" : @{
                 kXBorderColor_Key : kClearColor,
                 kXBorderWidth_Key : @(1),
                 
                 kXCornerRoundValue_Key : @(20),
                 },
             
             @"round-16" : @{
                 kXBorderColor_Key : kClearColor,
                 kXBorderWidth_Key : @(1),
                 
                 kXCornerRoundValue_Key : @(14),
                 },
             
             @"round-10" : @{
                 kXBorderColor_Key : kClearColor,
                 kXBorderWidth_Key : @(1),
                 
                 kXCornerRoundValue_Key : @(10),
                 },
             
             @"wildSand-bkg" : @{
                 kXBackgroundColor_Key : [XColors wildSand],
                 },
             
             };

#pragma clang diagnostic pop
}

//################################################################################
@end
//################################################################################
