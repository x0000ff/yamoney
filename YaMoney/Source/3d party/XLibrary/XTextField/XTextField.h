//
//  XTextField.h
//
//  Created by Konstantin Portnov on 11/12/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import <UIKit/UIKit.h>
#import "XStyles.h"
#import "XStylable.h"

//################################################################################
@interface XTextField : UITextField <XStylable>

//################################################################################
@property (strong, nonatomic) NSString * styleClass;

@property (assign, nonatomic) NSUInteger textInset;

@property (assign, nonatomic) CGFloat topLeftCornerRadius;
@property (assign, nonatomic) CGFloat topRightCornerRadius;
@property (assign, nonatomic) CGFloat bottomRightCornerRadius;
@property (assign, nonatomic) CGFloat bottomLeftCornerRadius;
@property (assign, nonatomic) CGFloat borderWidth;
@property (strong, nonatomic) UIColor * borderColor;

//################################################################################
- (void) setBorderWidth:(CGFloat)borderWidth;
- (void) setCornerRadius:(CGFloat)cornerRadius;

//################################################################################
@end
//################################################################################
