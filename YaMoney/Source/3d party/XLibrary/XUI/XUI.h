//
//  XUI.h
//
//  Created by Konstantin Portnov on 12/25/13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import <Foundation/Foundation.h>

//################################################################################
/*
 Cheats sheet
 http://fortawesome.github.io/Font-Awesome/cheatsheet/
 */

//################################################################################
@interface XUI : NSObject

//################################################################################
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button;
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color;
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color size:(CGFloat)size;
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color highlightedColor:(UIColor *)highlightedColor size:(CGFloat)size;
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color highlightedColor:(UIColor *)highlightedColor;

+ (void) applyIcon:(NSString *)iconString toImageView:(UIImageView *)imageView withColor:(UIColor *)color highlightedColor:(UIColor *)highlightedColor;
+ (void) applyIcon:(NSString *)iconString toImageView:(UIImageView *)imageView withColor:(UIColor *)color;

+ (UIImage *) icon:(NSString *)iconString withSize:(CGFloat)size color:(UIColor *)color;

//################################################################################
+ (UIBarButtonItem *) barButtonItemWithIcon:(NSString *)iconString color:(UIColor *)color target:(id)target selector:(SEL)selector;
+ (UIBarButtonItem *) barButtonItemWithIcon:(NSString *)iconString target:(id)target selector:(SEL)selector;

//################################################################################
@end
//################################################################################
