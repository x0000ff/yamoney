//
//  XUI.m
//
//  Created by Konstantin Portnov on 12/25/13.
//  Copyright (c) 2013 FLC-Team. All rights reserved.
//

//################################################################################
#import "XUI.h"
#import "FontAwesomeKit/FAKFontAwesome.h"

//################################################################################
@implementation XUI

//################################################################################
const CGFloat static kBarButtonImageSize = 20;
//################################################################################
#pragma mark - Icons

//################################################################################
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button
{
    [self applyIcon:iconString toButton:button withColor:nil];
}

//################################################################################
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color
{
    [self applyIcon:iconString toButton:button withColor:color size:button.bounds.size.height*0.75];
}

//################################################################################
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color highlightedColor:(UIColor *)highlightedColor
{
    [self applyIcon:iconString toButton:button withColor:color highlightedColor:highlightedColor size:button.bounds.size.height*0.75];
}

//################################################################################
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color size:(CGFloat)size
{
    [self applyIcon:iconString toButton:button withColor:color highlightedColor:nil size:size];
}

//################################################################################
+ (void) applyIcon:(NSString *)iconString toButton:(UIButton *)button withColor:(UIColor *)color highlightedColor:(UIColor *)highlightedColor size:(CGFloat)size
{
    if (!button) return;
    
    UIImage * icon = [XUI icon:iconString withSize:size color:color];
    [button setImage:icon forState:UIControlStateNormal];
    [button setTitle:nil forState:UIControlStateNormal];
    
    if ([button respondsToSelector:@selector(setTintColor:)]) {
        [button setTintColor:color];
    }
    
    if (highlightedColor) {
        UIImage * highlightedIcon = [XUI icon:iconString withSize:size color:highlightedColor];
        [button setImage:highlightedIcon forState:UIControlStateHighlighted];
        
    }
#ifdef DBG
    [button applyBorderWithColor:[UIColor redColor] width:1];
#endif
}

//################################################################################
+ (void) applyIcon:(NSString *)iconString toImageView:(UIImageView *)imageView withColor:(UIColor *)color
{
    [self applyIcon:iconString toImageView:imageView withColor:color highlightedColor:nil];
}

//################################################################################
+ (void) applyIcon:(NSString *)iconString toImageView:(UIImageView *)imageView withColor:(UIColor *)color highlightedColor:(UIColor *)highlightedColor
{
    if (![iconString nonEmpty]) {
        imageView.image = nil;
        return;
    }
    
    UIImage * icon = [XUI icon:iconString withSize:imageView.bounds.size.height color:color];
    imageView.image = icon;
    
    if (highlightedColor) {
        UIImage * highlightedIcon = [XUI icon:iconString withSize:imageView.bounds.size.height color:highlightedColor];
        imageView.highlightedImage = highlightedIcon;
    }
    
#ifdef DBG
    [imageView applyBorderWithColor:[UIColor redColor] width:1];
#endif
}

//################################################################################
+ (UIImage *) icon:(NSString *)iconString withSize:(CGFloat)size color:(UIColor *)color
{
    if (!color) color = [UIColor blackColor];
    
    FAKFontAwesome * icon = [FAKFontAwesome iconWithCode:iconString size:size];
    [icon addAttribute:NSForegroundColorAttributeName value:color];
    
    UIImage * iconImage = nil;
    iconImage = [icon imageWithSize:CGSizeMake(size, size)];

    return iconImage;
}

//################################################################################
+ (UIBarButtonItem *) barButtonItemWithIcon:(NSString *)iconString target:(id)target selector:(SEL)selector
{
    return [self barButtonItemWithIcon:iconString color:nil target:target selector:selector];
}

//################################################################################
+ (UIBarButtonItem *) barButtonItemWithIcon:(NSString *)iconString color:(UIColor *)color target:(id)target selector:(SEL)selector
{
    CGFloat size = kBarButtonImageSize;
    
    UIImage * icon = [XUI icon:iconString withSize:size color:color];
    UIBarButtonItem * barBtn = [[UIBarButtonItem alloc] initWithImage:icon style:UIBarButtonItemStylePlain target:target action:selector];
    if (color && [barBtn respondsToSelector:@selector(setTintColor:)])
    {
        [barBtn setTintColor:color];
    }
    
    return barBtn;
}

//################################################################################
@end
//################################################################################
