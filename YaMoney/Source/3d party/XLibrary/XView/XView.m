//
//  XView.m
//
//  Created by Konstantin Portnov on 11/12/13.
//  Copyright (c) 2013 DigevoLabs. All rights reserved.
//

//################################################################################
#import "XView.h"
#import "XStyles.h"
#import "UIImage+XUtilities.h"
#import "UIView+XUtilities.h"
#import <QuartzCore/QuartzCore.h>
#import "UIBezierPath+RoundedCornerPath.h"

//################################################################################
@implementation XView

//################################################################################
- (void) setStyleClass:(NSString *)styleClass
{
    if ([_styleClass isEqualToString:styleClass]) return;
    
    _styleClass = styleClass;
    [self stylize];
}

//################################################################################
- (id) initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        [self initialCustomization];
    }
    
    return self;
}

//################################################################################
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialCustomization];
    }
    
    return self;
}

//################################################################################
- (void) initialCustomization {
    
    _borderWidth = 0;
    
    _topLeftCornerRadius =      0;
    _topRightCornerRadius =     0;
    _bottomRightCornerRadius =  0;
    _bottomLeftCornerRadius =   0;
    
    _borderColor = nil;
}

//################################################################################
#pragma mark - Setters / Getters

//################################################################################
- (void) setHidden:(BOOL)hidden {
    
    [super setHidden:hidden];
    [self redrawMe];
}

//################################################################################
- (void) setCornerRadius:(CGFloat)cornerRadius {
    
    _topLeftCornerRadius =      cornerRadius;
    _topRightCornerRadius=      cornerRadius;
    _bottomRightCornerRadius =  cornerRadius;
    _bottomLeftCornerRadius =   cornerRadius;
    
    [self redrawMe];
}

//################################################################################
- (void) setTopLeftCornerRadius:(CGFloat)topLeftCornerRadius {
    
    _topLeftCornerRadius = topLeftCornerRadius;
    [self redrawMe];
}

//################################################################################
- (void) setTopRightCornerRadius:(CGFloat)topRightCornerRadius {
    
    _topRightCornerRadius = topRightCornerRadius;
    [self redrawMe];
}

//################################################################################
- (void) setBottomRightCornerRadius:(CGFloat)bottomRightCornerRadius {
    
    _bottomRightCornerRadius = bottomRightCornerRadius;
    [self redrawMe];
}

//################################################################################
- (void) setBottomLeftCornerRadius:(CGFloat)bottomLeftCornerRadius {
    
    _bottomLeftCornerRadius = bottomLeftCornerRadius;
    [self redrawMe];
}

//################################################################################
- (void) setBorderColor:(UIColor *)borderColor {
    
    _borderColor = borderColor;
    [self redrawMe];
}

//################################################################################
- (void) setBorderWidth:(CGFloat)borderWidth {
    
    _borderWidth = borderWidth;
    [self redrawMe];
}

//################################################################################
#pragma mark - Draw Logic

//################################################################################
- (void) redrawMe
{
    [self setNeedsDisplay];
}

//################################################################################
- (void)layoutSubviews {
    [super layoutSubviews];
    [self redrawMe];
}

//################################################################################
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//################################################################################
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (!self.hidden && self.borderColor && (self.borderWidth > 0)) {
        // get the context
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(ctx, self.backgroundColor.CGColor);
        CGContextFillRect(ctx, rect);
        
        CGContextSaveGState(ctx);
        
        const CGFloat lineWidth = self.borderWidth;
        
        CGRect insetRect = rect;
        CGRect radiuses = CGRectMake(
                                     self.topLeftCornerRadius,
                                     self.topRightCornerRadius,
                                     self.bottomRightCornerRadius,
                                     self.bottomLeftCornerRadius);
        
        UIBezierPath * bezierPath = [UIBezierPath createRoundedCornerPath:insetRect radiuses:radiuses];
        
        // set the stroke params
        CGContextSetStrokeColorWithColor(ctx, self.borderColor.CGColor);
        
        bezierPath.lineWidth = lineWidth;
        bezierPath.lineJoinStyle = kCGLineJoinRound;
        [bezierPath stroke];
        
        CAShapeLayer * mask = [CAShapeLayer layer];
        mask.frame = rect;
        mask.fillRule = kCAFillRuleNonZero;
        mask.path = [bezierPath CGPath];
        self.layer.mask = mask;
        self.layer.masksToBounds = YES;
    }
    else {
        self.layer.mask = nil;
        self.layer.masksToBounds = NO;
    }
}

//################################################################################
#pragma mark - Stylizable

//################################################################################
+ (NSArray *) stylers
{
    return @[
             XBackgroundColorStyler.class,
             XOpacityStyler.class,
             XTintStyler.class,
             XBorderStyler.class,
             XCornerStyler.class,
             ];
}

//################################################################################
- (void) stylize {
    
    for (Class stylerClass in [self.class stylers]) {
        [stylerClass style:self];
    }
}

//################################################################################
@end
//################################################################################
