//
//  AppDelegate.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "AppDelegate.h"

//##############################################################################
@interface AppDelegate ()

@end

//##############################################################################
@implementation AppDelegate

//##############################################################################
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

  [YMUI customizeUI];
  [DataLayer setup];
  return YES;
}

//##############################################################################
- (void)applicationWillTerminate:(UIApplication *)application {

  [DataLayer cleanup];
}

//##############################################################################
@end
//##############################################################################
