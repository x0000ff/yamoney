//
//  NSArray+SafeObjectAtIndex.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "NSArray+SafeObjectAtIndex.h"

//##############################################################################
@implementation NSArray (SafeObjectAtIndex)

//##############################################################################
- (id) safeObjectAtIndex:(NSUInteger)index {
  
  return (index < self.count) ? [self objectAtIndex:index] : nil;
}

//##############################################################################
@end
//##############################################################################
