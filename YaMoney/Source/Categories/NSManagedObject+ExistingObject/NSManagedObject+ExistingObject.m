//
//  NSManagedObject+ExistingObject.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "NSManagedObject+ExistingObject.h"

//##############################################################################
@implementation NSManagedObject (ExistingObject)

//##############################################################################
+ (id) existingObjectWithID:(NSManagedObjectID *)objectID {
  
  return [self existingObjectWithID:objectID inContext:[NSManagedObjectContext MR_defaultContext]];
}

//##############################################################################
+ (id) existingObjectWithID:(NSManagedObjectID *)objectID inContext:(NSManagedObjectContext *)context {
  
  if (!objectID) {
    return nil;
  }
  
  if (!context) {
    return nil;
  }

  return [[NSManagedObjectContext MR_context] existingObjectWithID:objectID error:nil];
}

//##############################################################################
@end
//##############################################################################
