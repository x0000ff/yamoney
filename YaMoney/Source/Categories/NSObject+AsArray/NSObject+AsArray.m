//
//  NSObject+AsArray.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "NSObject+AsArray.h"
#import "NSObject+AsInstanceOfClass.h"

//##############################################################################
@implementation NSObject (AsArray)

//##############################################################################
- (NSArray *) asArray {
  return [self asInstanceOfClass:NSArray.class];
}

//##############################################################################
- (NSMutableArray *) asMutableArray {
  return [self asInstanceOfClass:NSMutableArray.class];
}

//##############################################################################
@end
//##############################################################################
