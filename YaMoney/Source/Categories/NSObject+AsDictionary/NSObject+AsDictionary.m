//
//  NSObject+AsDictionary.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "NSObject+AsDictionary.h"
#import "NSObject+AsInstanceOfClass.h"

//##############################################################################
@implementation NSObject (AsDictionary)

//##############################################################################
- (NSDictionary *) asDictionary {
  return [self asInstanceOfClass:NSDictionary.class];
}

//##############################################################################
- (NSMutableDictionary *) asMutableDictionary {
  return [self asInstanceOfClass:NSMutableDictionary.class];
}
//##############################################################################
@end
//##############################################################################
