//
//  NSObject+AsInstanceOfClass.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "NSObject+AsInstanceOfClass.h"

//##############################################################################
@implementation NSObject (AsInstanceOfClass)

//##############################################################################
- (id) asInstanceOfClass:(Class)class {
  return [self isKindOfClass:class] ? self : nil;
}

//##############################################################################
@end
//##############################################################################
