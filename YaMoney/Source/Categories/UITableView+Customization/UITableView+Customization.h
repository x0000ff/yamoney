//
//  UITableView+Customization.h
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
@interface UITableView (Customization)

//##############################################################################
#pragma mark - Autoresizable Cells

- (void) enableAutoResizableCellsWithEstimatedRowHeight:(CGFloat)estimatedRowHeight;
- (void) enableAutoResizableCells;

//##############################################################################
#pragma mark - Empty Cells

- (void) enableHidingEmptyCells;

//##############################################################################
#pragma mark - Pull-To-Refresh

- (UIRefreshControl *) addPullToRefreshWithTarget:(id)target action:(SEL)action;
- (UIRefreshControl *) addRefreshControlWithTarget:(id)target action:(SEL)action;

//##############################################################################
@end
//##############################################################################
