//
//  UITableView+Customization.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "UITableView+Customization.h"

//##############################################################################
@implementation UITableView (Customization)

//##############################################################################
#pragma mark - Autoresizable Cells

//##############################################################################
- (void) enableAutoResizableCellsWithEstimatedRowHeight:(CGFloat)estimatedRowHeight {
 
  self.rowHeight = UITableViewAutomaticDimension;
  self.estimatedRowHeight = estimatedRowHeight;
}

//##############################################################################
- (void) enableAutoResizableCells {
  [self enableAutoResizableCellsWithEstimatedRowHeight:44];
}

//##############################################################################
#pragma mark - Empty Cells

//##############################################################################
- (void) enableHidingEmptyCells {

  // Hides empty cells
  self.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

//##############################################################################
#pragma mark - Pull-To-Refresh

//##############################################################################
- (UIRefreshControl *) addPullToRefreshWithTarget:(id)target action:(SEL)action {
  return [self addRefreshControlWithTarget:target action:action];
}

//##############################################################################
- (UIRefreshControl *) addRefreshControlWithTarget:(id)target action:(SEL)action {
  
  UITableViewController * tableViewController = [[UITableViewController alloc] init];
  tableViewController.tableView = self;
  
  UIRefreshControl * refreshControl = [[UIRefreshControl alloc] init];
  
  if (target && action) {
    [refreshControl addTarget:target action:action forControlEvents:UIControlEventValueChanged];
  }
  
  tableViewController.refreshControl = refreshControl;

  return refreshControl;
}

//##############################################################################
@end
//##############################################################################
