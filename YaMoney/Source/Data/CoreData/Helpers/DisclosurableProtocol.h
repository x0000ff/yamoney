//
//  DisclosurableProtocol.h
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
@protocol DisclosurableProtocol <NSObject>

- (BOOL) hasDisclosure;

@end
//##############################################################################
