//
//  SubCategoriesContainerProtocol.h
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import <CoreData/CoreData.h>

//##############################################################################
@protocol SubCategoriesContainerProtocol <NSObject>

- (BOOL)      hasSubCategories;
- (NSSet *)   subCategories;
- (NSArray *) sortedSubCategories;

@end
//##############################################################################
