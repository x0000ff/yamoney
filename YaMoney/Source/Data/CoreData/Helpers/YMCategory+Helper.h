//
//  YMCategory+Helper.h
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "YMCategory.h"
#import "SubCategoriesContainerProtocol.h"
#import "TitlableProtocol.h"
#import "DisclosurableProtocol.h"

//##############################################################################
@interface YMCategory (Helper) <TitlableProtocol, DisclosurableProtocol, SubCategoriesContainerProtocol>

//##############################################################################
+ (NSString *) orderColumnName;

//##############################################################################
#pragma mark - SubCategoriesContainerProtocol

//##############################################################################
- (NSArray *) sortedSubCategories;

//##############################################################################
@end
//##############################################################################
