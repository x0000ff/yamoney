//
//  YMSubCategory+Helper.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "YMSubCategory+Helper.h"

//##############################################################################
@implementation YMSubCategory (Helper)

//##############################################################################
+ (NSString *) categoryColumnName {
  return @"category";
}

//##############################################################################
+ (NSString *) parentSubCategoryColumnName {
  return @"parentSubCategory";
}

//##############################################################################
+ (NSString *) titleColumnName {
  return @"title";
}

//##############################################################################
#pragma mark - SubCategoriesContainerProtocol

//##############################################################################
- (BOOL) hasSubCategories {
  return self.subCategories.count > 0;
}

//##############################################################################
- (NSArray *) sortedSubCategories {
  
  NSPredicate * predicate = [NSPredicate predicateWithFormat:@"%K = %@", [YMSubCategory parentSubCategoryColumnName], self];
  NSArray * subCategories = [YMSubCategory MR_findAllSortedBy:[YMSubCategory titleColumnName] ascending:YES withPredicate:predicate];
  return subCategories;
}

//##############################################################################
#pragma mark - Titlable
// Nothing to implement "YMCategory" already has "title" property

//##############################################################################
#pragma mark - DisclosurableProtocol

//##############################################################################
- (BOOL) hasDisclosure {
  
  return [self hasSubCategories];
}

//##############################################################################
@end
//##############################################################################
