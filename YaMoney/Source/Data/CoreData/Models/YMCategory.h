//
//  YMCategory.h
//  YaMoney
//
//  Created by x0000ff on 12/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class YMSubCategory;

@interface YMCategory : NSManagedObject

@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *subCategories;
@end

@interface YMCategory (CoreDataGeneratedAccessors)

- (void)addSubCategoriesObject:(YMSubCategory *)value;
- (void)removeSubCategoriesObject:(YMSubCategory *)value;
- (void)addSubCategories:(NSSet *)values;
- (void)removeSubCategories:(NSSet *)values;

@end
