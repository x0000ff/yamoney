//
//  YMCategory.m
//  YaMoney
//
//  Created by x0000ff on 12/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

#import "YMCategory.h"
#import "YMSubCategory.h"


@implementation YMCategory

@dynamic order;
@dynamic title;
@dynamic subCategories;

@end
