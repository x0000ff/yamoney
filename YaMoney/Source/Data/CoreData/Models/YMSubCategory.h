//
//  YMSubCategory.h
//  YaMoney
//
//  Created by x0000ff on 12/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class YMCategory, YMSubCategory;

@interface YMSubCategory : NSManagedObject

@property (nonatomic, retain) NSNumber * scID;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) YMCategory *category;
@property (nonatomic, retain) NSSet *subCategories;
@property (nonatomic, retain) YMSubCategory *parentSubCategory;
@end

@interface YMSubCategory (CoreDataGeneratedAccessors)

- (void)addSubCategoriesObject:(YMSubCategory *)value;
- (void)removeSubCategoriesObject:(YMSubCategory *)value;
- (void)addSubCategories:(NSSet *)values;
- (void)removeSubCategories:(NSSet *)values;

@end
