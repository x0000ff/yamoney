//
//  YMSubCategory.m
//  YaMoney
//
//  Created by x0000ff on 12/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

#import "YMSubCategory.h"
#import "YMCategory.h"
#import "YMSubCategory.h"


@implementation YMSubCategory

@dynamic scID;
@dynamic title;
@dynamic category;
@dynamic subCategories;
@dynamic parentSubCategory;

@end
