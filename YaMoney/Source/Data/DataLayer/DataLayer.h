//
//  DataLayer.h
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import <Foundation/Foundation.h>
#import "Import-CoreDataModels.h"
#import "NSManagedObject+ExistingObject.h"

//##############################################################################
@interface DataLayer : NSObject

//##############################################################################
+ (void) setup;
+ (void) cleanup;

//##############################################################################
+ (void) importCategories:(NSArray *)categories callback:(void(^)(BOOL success, NSArray * importedObjects))callback;

//##############################################################################
@end
//##############################################################################
