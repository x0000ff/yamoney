//
//  DataLayer.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "DataLayer.h"

//##############################################################################
@implementation DataLayer

//##############################################################################
#pragma mark - Setup / Cleanup

//##############################################################################
+ (void) setup {

  [MagicalRecord setupAutoMigratingCoreDataStack];
  [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelError];
}

//##############################################################################
+ (void) cleanup {
  
  [MagicalRecord cleanUp];
}

//##############################################################################
#pragma mark - Imports

//################################################################################
//+ (void) importCategories:(NSArray *)categories callback:(void(^)(BOOL success, NSArray * importedObjects))callback {
//  
//  Class objectClass = YMCategory.class;
//  NSArray * jsonObjects = [categories asArray];
//  __block NSArray * importedObjects = nil;
//  
//  [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//
//    [objectClass MR_truncateAllInContext:localContext];
//    importedObjects = [objectClass MR_importFromArray:jsonObjects inContext:localContext];
//    
//  } completion:^(BOOL contextDidSave, NSError *error) {
//    if (callback) { callback(contextDidSave, importedObjects); }
//  }];
//}

//################################################################################
+ (void) importCategories:(NSArray *)categories callback:(void(^)(BOOL success, NSArray * importedObjects))callback {
  
  Class objectClass = YMCategory.class;
  NSArray * jsonObjects = [categories asArray];
  __block NSMutableArray * importedObjects = [NSMutableArray new];
  
  [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
    
    [objectClass MR_truncateAllInContext:localContext];
    
    for (int i = 0; i < jsonObjects.count; i++) {
      
      NSDictionary * jsonObject = jsonObjects[i];
      
      YMCategory * category = [YMCategory MR_importFromObject:jsonObject inContext:localContext];
      category.order = @(i);
      
      if (category) { [importedObjects addObject:category]; };
    }
    
  } completion:^(BOOL contextDidSave, NSError *error) {
    if (callback) { callback(contextDidSave, importedObjects); }
  }];
}

//##############################################################################
@end
//##############################################################################
