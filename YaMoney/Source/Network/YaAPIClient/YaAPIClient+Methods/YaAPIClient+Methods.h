//
//  YaAPIClient+Methods.h
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "YaAPIClient.h"

//##############################################################################
@interface YaAPIClient (Methods)

//##############################################################################
+ (void) getCategoriesWithCallback:(void(^)(BOOL success, NSArray * categories, NSError * error))callback;

//##############################################################################
@end
//##############################################################################
