//
//  YaAPIClient+Methods.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "YaAPIClient+Methods.h"

//##############################################################################
@implementation YaAPIClient (Methods)

//##############################################################################
+ (void) getCategoriesWithCallback:(void(^)(BOOL success, NSArray * categories, NSError * error))callback {

  [[self sharedClient] GET:@"/v2/59a035eb1100006a006441da" parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
    
    __weak NSArray * categories = [json asArray];
    
    [DataLayer importCategories:categories callback:^(BOOL success, NSArray * importedObjects) {
      if (callback) { callback(YES, importedObjects, nil); }
    }];
    
  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

    if (callback) { callback(NO, nil, error); }
  
  }];
  
}

//##############################################################################
@end
//##############################################################################
