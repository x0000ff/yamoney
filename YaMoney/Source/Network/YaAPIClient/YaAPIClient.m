//
//  YaAPIClient.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "YaAPIClient.h"
#import "AFHTTPRequestOperationLogger.h"
#import "YaAPIClient-Constants.h"

//##############################################################################
@implementation YaAPIClient

//################################################################################
#pragma mark - Singleton

//################################################################################
+ (YaAPIClient *)sharedClient {
  
  static YaAPIClient * _sharedClient = nil;
  static dispatch_once_t onceToken;
  
  dispatch_once(&onceToken, ^{
    
    _sharedClient = [[YaAPIClient alloc] initWithBaseURL:[NSURL URLWithString:[self apiBaseURLString]]];
    
    AFJSONResponseSerializer * responseSerializer = [[AFJSONResponseSerializer alloc] init];
    _sharedClient.responseSerializer = responseSerializer;
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                            diskCapacity:0
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    
    [self setupLogging];
  });
  
  return _sharedClient;
}

//################################################################################
#pragma mark - Configuration Helpers

//################################################################################
+ (NSString *) apiBaseURLString {
  
  return kYMAPIBaseURL;
}

//################################################################################
+ (void) setupLogging {

  [[AFHTTPRequestOperationLogger sharedLogger] startLogging];
  
#ifdef DEBUG
  [[AFHTTPRequestOperationLogger sharedLogger] setLevel:AFLoggerLevelDebug];
#else
  [[AFHTTPRequestOperationLogger sharedLogger] setLevel:AFLoggerLevelOff];
#endif
}

//##############################################################################
@end
//##############################################################################
