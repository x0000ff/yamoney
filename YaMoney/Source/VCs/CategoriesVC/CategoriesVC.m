//
//  CategoriesVC.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "CategoriesVC.h"
#import "SubCategoriesVC.h"
#import "TitlableTitleCell.h"

//##############################################################################
@interface CategoriesVC () <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView * tableView;
@property (strong, nonatomic) UIRefreshControl * refreshControl;

@end

//##############################################################################
@implementation CategoriesVC

//##############################################################################
static NSString * const kSubCategoriesSegureIdentifier = @"SubCategories";

//##############################################################################
#pragma mark - Overrides

//##############################################################################
- (void)viewDidLoad {

  [super viewDidLoad];

  [self setupUI];
}

//##############################################################################
- (void) viewWillAppear:(BOOL)animated {
  
  [super viewWillAppear:animated];
  
  if ([YMCategory MR_countOfEntities] == 0) {
    [self loadCategories];
  } else {
    [self updateUI];
  }
}

//##############################################################################
#pragma mark - UI

//##############################################################################
- (void) setupUI {

  self.title = NSLocalizedString(@"Categories", nil);
  [self setupTableView];
}

//##############################################################################
- (void) setupTableView {
  
  // TableView
  [self.tableView enableAutoResizableCells];
  
  // Hides empty cells
  [self.tableView enableHidingEmptyCells];
  
  // Setup Pull-To-Refresh
  self.refreshControl = [self.tableView addPullToRefreshWithTarget:self action:@selector(refresh:)];
  self.refreshControl.tintColor = [XColors midGray];
}

//##############################################################################
- (void) updateUI {
  
  [self.tableView reloadData];
}

//##############################################################################
#pragma mark - Network

//##############################################################################
- (void) loadCategories {

  [self.view showHudWithActivity:YES];
  
  __weak CategoriesVC * weakSelf = self;
  
  [YaAPIClient getCategoriesWithCallback:^(BOOL success, NSArray * categories, NSError *error) {
    
    [weakSelf.view hideAllHudsAnimated:NO];
    
    if (!success) {
      [[weakSelf.view showFailHudWithText:NSLocalizedString(@"Error", nil) detailsText:error.localizedDescription] hide:YES afterDelay:kHudDelay_Short];
    }
    
    [weakSelf updateUI];
  }];

}

//##############################################################################
#pragma mark - Data

//##############################################################################
- (NSArray *) categories {
  return [YMCategory MR_findAllSortedBy:[YMCategory orderColumnName] ascending:YES];
}

//##############################################################################
- (YMCategory *) categoryAtIndexPath:(NSIndexPath *)indexPath {
  
  return [[self categories] safeObjectAtIndex:indexPath.row];
}

//##############################################################################
#pragma mark - Actions

//##############################################################################
- (IBAction) refresh:(id)sender {
  
  [self.refreshControl endRefreshing];
  
  [self loadCategories];
}

//##############################################################################
#pragma mark - UITableViewDataSource

//##############################################################################
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self categories].count;
}

//##############################################################################
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  TitlableTitleCell * cell = [[tableView dequeueReusableCellWithIdentifier:[TitlableTitleCell reuseIdentifier] forIndexPath:indexPath] asInstanceOfClass:TitlableTitleCell.class];
  
  YMCategory * category = [self categoryAtIndexPath:indexPath];
  [cell configureWithObject:category];
  
  return cell;
}

//##############################################################################
#pragma mark - Navigation

//##############################################################################
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
  if ([segue.identifier isEqualToString:kSubCategoriesSegureIdentifier]) {
   
    NSIndexPath * selectedIndexPath = self.tableView.indexPathForSelectedRow;
    YMCategory * category = [self categoryAtIndexPath:selectedIndexPath];
    
    SubCategoriesVC * vc = [segue.destinationViewController asInstanceOfClass:SubCategoriesVC.class];
    vc.parentItemID = category.objectID;
  }
  
}
//##############################################################################
@end
//##############################################################################
