//
//  SubCategoriesVC.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "SubCategoriesVC.h"
#import "TitlableTitleCell.h"

//##############################################################################
@interface SubCategoriesVC () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView * tableView;

@end

//##############################################################################
@implementation SubCategoriesVC

//##############################################################################
#pragma mark - Overrides

//##############################################################################
- (void)viewDidLoad {

  [super viewDidLoad];

  [self setupUI];
}

//##############################################################################
- (void) viewWillAppear:(BOOL)animated {
  
  [super viewWillAppear:animated];
  
  [self updateUI];
}

//##############################################################################
#pragma mark - UI

//##############################################################################
- (void) setupUI {
  
  [self setupTableView];
}

//##############################################################################
- (void) setupTableView {
  
  // TableView
  [self.tableView enableAutoResizableCells];
  [self.tableView enableHidingEmptyCells];
}

//##############################################################################
- (void) updateUI {
  
  self.title = [self parentItem].title;
  [self.tableView reloadData];
}

//##############################################################################
#pragma mark - Data

//##############################################################################
- (NSManagedObject<TitlableProtocol, SubCategoriesContainerProtocol> *) parentItem {
  
  NSManagedObject * parentItem = [NSManagedObject existingObjectWithID:self.parentItemID];
  
  if ([parentItem conformsToProtocol:@protocol(TitlableProtocol)] && [parentItem conformsToProtocol:@protocol(SubCategoriesContainerProtocol)]) {
    return (NSManagedObject<TitlableProtocol, SubCategoriesContainerProtocol> *)parentItem;
  }
  
  return nil;
}

//##############################################################################
- (NSArray *) subCategories {
  return [[self parentItem] sortedSubCategories];
}

//##############################################################################
- (YMSubCategory *) subCategoryAtIndexPath:(NSIndexPath *)indexPath {
  
  return [[self subCategories] safeObjectAtIndex:indexPath.row];
}

//##############################################################################
#pragma mark - UITableViewDataSource

//##############################################################################
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self subCategories].count;
}

//##############################################################################
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  TitlableTitleCell * cell = [[tableView dequeueReusableCellWithIdentifier:[TitlableTitleCell reuseIdentifier] forIndexPath:indexPath] asInstanceOfClass:TitlableTitleCell.class];
  
  YMSubCategory * subCategory = [self subCategoryAtIndexPath:indexPath];
  
  [cell configureWithObject:subCategory];
  
  return cell;
}

//##############################################################################
#pragma mark - UITableViewDelegate

//##############################################################################
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
  YMSubCategory * subCategory = [self subCategoryAtIndexPath:indexPath];
  if ([subCategory hasSubCategories]) {
    [self showSubCategoriesForSubCategory:subCategory];
  }
  
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//##############################################################################
#pragma mark - Navigation

//##############################################################################
- (void) showSubCategoriesForSubCategory:(YMSubCategory *)subCategory {
  
  SubCategoriesVC * vc = [[self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(SubCategoriesVC.class)] asInstanceOfClass:SubCategoriesVC.class];
  vc.parentItemID = subCategory.objectID;

  [self.navigationController pushViewController:vc animated:YES];
  
}

//##############################################################################
@end
//##############################################################################
