//
//  TitlableTitleCell.h
//  YaMoney
//
//  Created by x0000ff on 17/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import <UIKit/UIKit.h>
#import "TitleCell.h"
#import "TitlableProtocol.h"
#import "DisclosurableProtocol.h"

//##############################################################################
@interface TitlableTitleCell : TitleCell

//##############################################################################
- (void) configureWithObject:(NSObject<TitlableProtocol, DisclosurableProtocol> *)object;

//##############################################################################
@end
//##############################################################################
