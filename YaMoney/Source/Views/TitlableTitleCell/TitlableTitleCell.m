//
//  TitlableTitleCell.m
//  YaMoney
//
//  Created by x0000ff on 17/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "TitlableTitleCell.h"

//##############################################################################
@interface TitlableTitleCell ()

@end

//##############################################################################

//##############################################################################
@implementation TitlableTitleCell

//##############################################################################
- (void) awakeFromNib {
  
  [super awakeFromNib];
}

//##############################################################################
- (void) configureWithObject:(NSObject<TitlableProtocol, DisclosurableProtocol> *)object {

  [self configureWithTitle:[object title] hasDisclosure:[object hasDisclosure]];
}

//##############################################################################
@end
//##############################################################################
