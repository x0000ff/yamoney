//
//  TitleCell.m
//  YaMoney
//
//  Created by x0000ff on 17/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "TitleCell.h"

//##############################################################################
@interface TitleCell ()

@end

//##############################################################################

//##############################################################################
@implementation TitleCell

//##############################################################################
- (void) awakeFromNib {
  
  [super awakeFromNib];
}

//##############################################################################
- (void) configureWithTitle:(NSString *)title hasDisclosure:(BOOL)hasDisclosure {
  
  self.textLabel.text = title;
  self.accessoryType = hasDisclosure ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
}

//##############################################################################
@end
//##############################################################################
