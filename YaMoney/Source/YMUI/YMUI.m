//
//  YMUI.m
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//##############################################################################
#import "YMUI.h"

//##############################################################################
@implementation YMUI

//##############################################################################
+ (void) customizeUI {

  [self customizeNavigationBarAppearance];
}

//################################################################################
+ (UIFont *) defaultFontForSize:(CGFloat)size
{
  return [UIFont fontWithName:@"Arial" size:size];
}

//################################################################################
+ (void) customizeNavigationBarAppearance {
  
  UIColor * textColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
  UIColor * bkgColor = [XColors sunglow];
  UIImage * bkg = [UIImage imageWithColor:bkgColor];
  
  [[UINavigationBar appearance] setBackgroundImage:bkg forBarMetrics:UIBarMetricsDefault];
  CGFloat fontSize = 18;
  
  UIFont * font = [self defaultFontForSize:fontSize];
  
  if([[UINavigationBar class] instancesRespondToSelector:@selector(setBarTintColor:)]) {
    [[UINavigationBar appearance] setBarTintColor:textColor];
    
    [[UINavigationBar appearance] setTintColor:textColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSFontAttributeName : font,
                                                           NSForegroundColorAttributeName:textColor,
                                                           }];
  }
  else
    {
    [[UINavigationBar appearance] setTintColor:bkgColor];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSFontAttributeName : font,
                                                           NSForegroundColorAttributeName:textColor
                                                           }];
    }
  
  [[UIBarButtonItem appearance] setTintColor:textColor];
}

//##############################################################################
@end
//##############################################################################
