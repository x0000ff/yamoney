//
//  Libs.pch
//  YaMoney
//
//  Created by x0000ff on 06/08/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

#ifndef YaMoney_Libs_pch
#define YaMoney_Libs_pch

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.
#import <MagicalRecord/MagicalRecord.h>
#undef MR_LOGGING_DISABLED
#undef MR_ENABLE_ACTIVE_RECORD_LOGGING

#import "XLibrary.h"

#endif
